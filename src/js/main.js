AOS.init();
$(window).scroll(function(){
    if ($(window).scrollTop() > 05)
        $(".navbar").css("background-color","rgba(0, 0, 0, 0.60)");
    else
        $(".navbar").css("background-color","");
});

$(".img-container > .img").mousemove(function(event){
    var mousex = event.pageX - $(this).offset().left;
    var mousey = event.pageY - $(this).offset().top;
    
    var imgx = (mousex - 300) / 40;
    var imgy = (mousey - 200) / 40;
    
    $(this).css("transform", "translate(" + imgx + "px," + imgy + "px)");
});

$(".img-container > .img").mouseout(function(){
    $(this).css("transform", "translate(0px,0px)");
});

var options = {
    strings: ["<b>WE ARE ZION MINISTRY.</b>"],
    typeSpeed: 120,
    backSpeed: 120,
    backDelay: 500,
    startDelay: 1000,
    loop: false
}
  
var typed = new Typed(".zion-container > h1", options);